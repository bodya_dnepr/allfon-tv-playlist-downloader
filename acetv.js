const download = async function() {
  const promise = new Promise(function(resolve, reject){
    const url = 'http://acetv.org/js/data.json'
    let body = ''
    require('http').get(url, res => {
      res.setEncoding('utf8')
      res.on('data', data => body += data)
      res.on("end", _ => resolve(JSON.parse(body)))
    })
  })
  const response = await promise
  const template = ch => [
    `#EXTINF:0 group-title="${ch.cat}",${ch.name}`, `acestream://${ch.url}`
  ].join('\n')
  const m3u = ['#EXTM3U'].concat(response.channels.map(template)).join('\n')
  const fs = require('fs')
  fs.writeFileSync('/storage/downloads/acestream.m3u', m3u)
}
download()
