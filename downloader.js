const request = require('request-promise-native')
const cheerio = require('cheerio')
const queue = require('queue')({concurrency: 3})
const fs = require('fs')

const playlistArray = ['#EXTM3U']
const parsed = []
const options = url => { return {
  uri: `http://allfon-tv.com${url}`,
  transform: (body) => cheerio.load(body)
}}

const parseChannels = $ => {
  const article = $('#vertical_tab_nav > div > article:first-child')
  return article.find('figure.img').map((i, el) => {
    const url = el.firstChild.attribs.href
    const hash = `#${url.split('#')[1]}`
    const name = el.lastChild.lastChild.data
    return {url: url, name: name, categoryId: hash}
  })
}

const channelRq = (index, ch) => {
  console.log(`requesting ${ch.name}`)
  return request(options(ch.url)).then($$ => {
    const groupName = $$(`${ch.categoryId} > a`).text()
    const script = $$('.tv-player-wrapper').find('script')[0].firstChild.data
    const regexp = /acestream:\/\/(.{40})"/
    const acestreamHash = regexp.exec(script)[1]
    parsed[index] = {name: ch.name, group: groupName, hash: acestreamHash}
  })
}

const savePlaylist = () => {
  parsed.forEach(c => {
    playlistArray.push(`#EXTINF:0 group-title="${c.group}",${c.name}`)
    playlistArray.push(`http://127.0.0.1:8000/pid/${c.hash}/stream.mp4`)
  })
  const playlistString = playlistArray.join('\n')
  fs.writeFileSync('acestream.m3u', playlistString)
}

request(options('')).then($ => {
  const channels = parseChannels($)
  channels.map((index, ch) => queue.push(cb => channelRq(index, ch)))

  queue.start(function (err) {
    if(err) throw err
    savePlaylist()
    console.log('acestream.m3u saved')
  })

}).catch((err) => console.error(err))
